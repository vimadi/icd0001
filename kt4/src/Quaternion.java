import java.util.*;

import static java.lang.StrictMath.abs;

public class Quaternion {

   private static final double DELTA = 0.000001;
   private double a;
   private double b;
   private double c;
   private double d;

   public Quaternion (double a, double b, double c, double d) {
      this.a = a;
      this.b = b;
      this.c = c;
      this.d = d;
   }

   public double getRpart() {
      return a;
   }

   public double getIpart() {
      return b;
   }

   public double getJpart() {
      return c;
   }

   public double getKpart() {
      return d;
   }

   @Override
   public String toString() {
      StringBuilder str = new StringBuilder();
      Double[] doubleList = new Double[]{a, b, c, d};
      String[] charList = new String[]{"", "i", "j", "k"};
      for (int i = 0; i < doubleList.length; i++) {
         int y = (int) (double) doubleList[i];
         if (i != 0 && y >= 0) {
            str.append("+");
         }
         str.append(y)
            .append(charList[i]);
      }
      return str.toString();
   }

   public static Quaternion valueOf (String s) {
      ArrayList<Double> doubleList = new ArrayList<>();
      s = s.replaceAll("[ijk]", "");
      StringBuilder newstr = new StringBuilder();
      for (int i = 0; i < s.length(); i++) {
         char char1 = s.charAt(i);
         if (char1 == '+' || char1 == '-') {
            if (newstr.length() != 0) {
               doubleList.add(Double.valueOf(newstr.toString()));
               newstr = new StringBuilder();
            }
            newstr = new StringBuilder(String.valueOf(char1));
         } else if (Character.isDigit(char1)) {
            newstr.append(char1);
         }
      }
      doubleList.add(Double.valueOf(newstr.toString()));
      return new Quaternion(doubleList.get(0), doubleList.get(1), doubleList.get(2), doubleList.get(3));
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      return new Quaternion(a, b, c, d);
   }

   public boolean isZero() {

      return a < DELTA && a > -abs(DELTA) &&
              b < DELTA && b > -abs(DELTA) &&
              c < DELTA && c > -abs(DELTA) &&
              d < DELTA && d > -abs(DELTA);
   }

   public Quaternion conjugate() {
      return new Quaternion(a, -b, -c, -d);
   }

   public Quaternion opposite() {
      return new Quaternion(-a, -b, -c, -d);
   }

   public Quaternion plus (Quaternion q) {
      return new Quaternion(a + q.a, b + q.b, c + q.c, d + q.d);
   }

   public Quaternion times (Quaternion q) {
      return new Quaternion(
              a * q.a - b * q.b - c * q.c - d * q.d,
              a * q.b + b * q.a + c * q.d - d * q.c,
              a * q.c - b * q.d + c * q.a + d * q.b,
              a * q.d + b * q.c - c * q.b + d * q.a);
   }

   public Quaternion times (double r) {
      return new Quaternion(a * r, b * r, c * r, d * r);
   }

   public Quaternion inverse() {
      if (isZero()) {
         throw new RuntimeException("Quaternion values can't be zeros to inverse");
      }
      return new Quaternion(a/(a*a+b*b+c*c+d*d), -b/(a*a+b*b+c*c+d*d), -c/(a*a+b*b+c*c+d*d), -d/(a*a+b*b+c*c+d*d));
   }

   public Quaternion minus (Quaternion q) {
      return new Quaternion(a - q.a, b - q.b, c - q.c, d - q.d);
   }

   public Quaternion divideByRight (Quaternion q) {
      if (q.isZero()) {
         throw new RuntimeException("Quaternion values can't be zeros to divideByRight");
      }
      return times(q.inverse());
   }

   public Quaternion divideByLeft (Quaternion q) {
      if (q.isZero()) {
         throw new RuntimeException("Quaternion values can't be zeros to divideByLeft");
      }
      return q.inverse().times(this);
   }

   @Override
   public boolean equals (Object qo) {
         Quaternion qoa2 = (Quaternion) qo;
         return new Quaternion(
                 abs(a) - abs(qoa2.a), abs(b) - abs(qoa2.b), abs(c) - abs(qoa2.c), abs(d) - abs(qoa2.d))
                 .isZero();
   }

   public Quaternion dotMult (Quaternion q) {
      Quaternion quaternion = times(q.conjugate()).plus(q.times(conjugate()));
      quaternion.a /= 2;
      quaternion.b /= 2;
      quaternion.c /= 2;
      quaternion.d /= 2;
      return quaternion;
   }

   @Override
   public int hashCode() {
      return toString().hashCode();
   }

   public double norm() {
      return Math.sqrt(a*a+b*b+c*c+d*d);
   }

   public static void main (String[] arg) {
      Quaternion arv1 = new Quaternion (-1., 1, 2., -2.);
      if (arg.length > 0)
         arv1 = valueOf (arg[0]);
      System.out.println ("first: " + arv1.toString());
      System.out.println ("real: " + arv1.getRpart());
      System.out.println ("imagi: " + arv1.getIpart());
      System.out.println ("imagj: " + arv1.getJpart());
      System.out.println ("imagk: " + arv1.getKpart());
      System.out.println ("isZero: " + arv1.isZero());
      System.out.println ("conjugate: " + arv1.conjugate());
      System.out.println ("opposite: " + arv1.opposite());
      System.out.println ("hashCode: " + arv1.hashCode());
      Quaternion res = null;
      try {
         res = (Quaternion)arv1.clone();
      } catch (CloneNotSupportedException e) {};
      System.out.println ("clone equals to original: " + res.equals (arv1));
      System.out.println ("clone is not the same object: " + (res!=arv1));
      System.out.println ("hashCode: " + res.hashCode());
      res = valueOf (arv1.toString());
      System.out.println ("string conversion equals to original: " 
         + res.equals (arv1));
      Quaternion arv2 = new Quaternion (1., -2.,  -1., 2.);
      if (arg.length > 1)
         arv2 = valueOf (arg[1]);
      System.out.println ("second: " + arv2.toString());
      System.out.println ("hashCode: " + arv2.hashCode());
      System.out.println ("equals: " + arv1.equals (arv2));
      res = arv1.plus (arv2);
      System.out.println ("plus: " + res);
      System.out.println ("times: " + arv1.times (arv2));
      System.out.println ("minus: " + arv1.minus (arv2));
      double mm = arv1.norm();
      System.out.println ("norm: " + mm);
      System.out.println ("inverse: " + arv1.inverse());
      System.out.println ("divideByRight: " + arv1.divideByRight (arv2));
      System.out.println ("divideByLeft: " + arv1.divideByLeft (arv2));
      System.out.println ("dotMult: " + arv1.dotMult (arv2));
   }
}
// end of file
