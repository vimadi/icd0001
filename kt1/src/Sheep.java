//Sain abi lehelt: https://www.geeksforgeeks.org/sort-array-containing-two-types-elements/

public class Sheep {
   enum Animal {sheep, goat};
   
   public static void main (String[] param) {
      // test
   }

   public static void reorder (Animal[] animals) {
      int lCounter = 0;
      int rCounter = animals.length - 1;

      while (lCounter < rCounter) {
         if (animals[lCounter] == Animal.sheep) {
            animals[lCounter] = animals[rCounter];
            animals[rCounter] = Animal.sheep;
            rCounter--;
         } else {
            lCounter++;
         }
      }
   }
}

