import java.util.LinkedList;
import java.util.NoSuchElementException;

public class LongStack {

   private LinkedList<Long> linkedList = new LinkedList<>();

   public static void main(String args[]) {
      String s = "2 5 SWAP -";
      LongStack longStack = new LongStack();
      System.out.print(longStack.interpret (s));
   }

   @Override
   public Object clone() throws CloneNotSupportedException {
      LongStack longStack = new LongStack();
      longStack.linkedList.addAll(linkedList);
      return longStack;
   }

   public boolean stEmpty() {
      return linkedList.isEmpty();
   }

   public void push (long a) {
      linkedList.addLast(a);
   }

   public long pop() {
      long last;
      try {
         last = linkedList.getLast();
      } catch (NoSuchElementException e) {
         throw new RuntimeException("There are not enough numbers in the input");
      }
      linkedList.removeLast();
      return last;
   }

   public void op (String s) {
      long last;
      long first;
      long third;
      try {
         last = linkedList.getLast();
         linkedList.removeLast();
         first = linkedList.getLast();
         linkedList.removeLast();
      } catch (NoSuchElementException e) {
         if (s.equals("SWAP")) {
            throw new RuntimeException("There are not enough element to swap");
         }
         throw new RuntimeException("There are not enough numbers in the input");
      }
      switch (s) {
         case "+":
            push(first + last);
            break;
         case "-":
            push(first - last);
            break;
         case "*":
            push(first * last);
            break;
         case "/":
            push(first / last);
            break;
         case "SWAP":
            push(last);
            push(first);
            break;
         case "ROT":
            try {
               third = pop();
            } catch (RuntimeException e) {
               throw new RuntimeException("There are not enough elements to rot");
            }
            push(first);
            push(last);
            push(third);
            break;
         default:
            throw new IllegalArgumentException("Illegal symbol "+s);

      }
   }
  
   public long tos() throws RuntimeException {
      try {
         return linkedList.getLast();
      } catch (NoSuchElementException e) {
         throw new RuntimeException("There are not enough numbers in the input");
      }
   }

   @Override
   public boolean equals (Object o) {
      return linkedList.equals(((LongStack) o).linkedList);
   }

   @Override
   public String toString() {
      StringBuilder sb = new StringBuilder();
      for (int i = 0; i < linkedList.size(); i++) {
         sb.append(linkedList.getFirst());
      }
      return String.valueOf(sb);
   }

   public static long interpret (String pol) {
      if (pol == null) {
         throw new RuntimeException("Empty expression");
      }
      String[] arrOfStr = pol.split("[\t ]");
      LongStack longStack = new LongStack();
      for (String s : arrOfStr) {
         if (s.equals("")) {
            continue;
         }
         try {
            longStack.push(Long.parseLong(s));
         } catch (NumberFormatException e) {
            try {
               longStack.op(s);
            } catch (IllegalArgumentException i){
               throw new RuntimeException(i + " in expression "+'"'+pol+'"');
            } catch (RuntimeException j) {
               throw new RuntimeException("Can't perform " + s + " in expression " + '"' + pol + '"');
            }
         }
      }
      int size = longStack.linkedList.size();
      if (size > 1) {
         throw new RuntimeException("Too many numbers in expression "+'"'+pol+'"');
      } else if (size == 0) {
         throw new RuntimeException("Empty expression");
      }
      return longStack.linkedList.getLast();
   }

}
